using System.Diagnostics.CodeAnalysis;
using ExcentOne.Core.Security.Cryptography;
using NUnit.Framework;

namespace ExcentOne.Core.Security.Test
{
    [TestFixture]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public class TestEncryption
    {
        internal static string HashKey = "CvcHFclKOfGJxNDqzfHDtg24iDrXfNeanHK0UdVTa6s=";
        internal static string StringToEncrypt = "supercalifragilisticexpialidocious";
        internal static string EncryptedResult = "zit88ZZNBz/iEMMFAbOMaB043UswNVHlCEakrg/I6nw4V0nOprLGGw==";

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Successful_String_Encryption()
        {
            Assert.AreEqual(StringToEncrypt.Encrypt(HashKey), EncryptedResult);
        }
    }
}