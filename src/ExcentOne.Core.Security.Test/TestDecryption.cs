using System.Diagnostics.CodeAnalysis;
using ExcentOne.Core.Security.Cryptography;
using NUnit.Framework;

namespace ExcentOne.Core.Security.Test
{
    [TestFixture]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public class TestDecryption
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Successful_String_Encryption()
        {
            var input = TestEncryption.EncryptedResult.Decrypt(TestEncryption.HashKey);
            var expected = TestEncryption.StringToEncrypt;
            Assert.AreEqual(input, expected);
        }
    }
}