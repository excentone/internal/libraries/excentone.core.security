﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;
using System.Text;
using ExcentOne.Extensions.Text;

namespace ExcentOne.Core.Security.Cryptography
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public static class CryptographyExtensions
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum HashAlgorithmType
        {
            SHA1,
            SHA256,
            SHA384,
            SHA512,
            MD5
        }

        public static string Hash(this string plainText, string salt)
        {
            var bytes = Encoding.ASCII.GetBytes(salt);
            return Hash(plainText, HashAlgorithmType.SHA512, bytes);
        }

        public static bool VerifyHash(this string plainText, string hashString)
        {
            return VerifyHash(plainText, HashAlgorithmType.SHA512, hashString);
        }

        [SuppressMessage("ReSharper", "RedundantCaseLabel")]
        public static string Hash(this string plainText,
            HashAlgorithmType hashAlgorithm,
            byte[] saltBytes)
        {
            if (saltBytes == null)
            {
                const int minSaltSize = 4;
                const int maxSaltSize = 8;
                var random = new Random();
                var saltSize = random.Next(minSaltSize, maxSaltSize);
                saltBytes = new byte[saltSize];
                var rng = new RNGCryptoServiceProvider();
                rng.GetNonZeroBytes(saltBytes);
            }

            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var plainTextWithSaltBytes =
                new byte[plainTextBytes.Length + saltBytes.Length];

            for (var i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            for (var i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

            HashAlgorithm hash;
            switch (hashAlgorithm)
            {
                case HashAlgorithmType.SHA1:
                    hash = new SHA1Managed();
                    break;
                case HashAlgorithmType.SHA256:
                    hash = new SHA256Managed();
                    break;
                case HashAlgorithmType.SHA384:
                    hash = new SHA384Managed();
                    break;
                case HashAlgorithmType.SHA512:
                    hash = new SHA512Managed();
                    break;
                case HashAlgorithmType.MD5:
                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            var hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            var hashWithSaltBytes = new byte[hashBytes.Length +
                                             saltBytes.Length];

            for (var i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            for (var i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            var hashValue = Convert.ToBase64String(hashWithSaltBytes);

            return hashValue;
        }

        [SuppressMessage("ReSharper", "RedundantCaseLabel")]
        public static bool VerifyHash(string plainText,
            HashAlgorithmType hashAlgorithm,
            string hashValue)
        {
            var expectedHashString = plainText.UnHash(hashAlgorithm, hashValue);
            return hashValue == expectedHashString;
        }

        public static string UnHash(this string plainText, string hashValue)
        {
            return UnHash(plainText, HashAlgorithmType.SHA512, hashValue);
        }

        [SuppressMessage("ReSharper", "RedundantCaseLabel")]
        private static string UnHash(this string plainText, HashAlgorithmType hashAlgorithm, string hashValue)
        {
            var hashWithSaltBytes = Convert.FromBase64String(hashValue);
            int hashSizeInBits;
            switch (hashAlgorithm)
            {
                case HashAlgorithmType.SHA1:
                    hashSizeInBits = 160;
                    break;
                case HashAlgorithmType.SHA256:
                    hashSizeInBits = 256;
                    break;
                case HashAlgorithmType.SHA384:
                    hashSizeInBits = 384;
                    break;
                case HashAlgorithmType.SHA512:
                    hashSizeInBits = 512;
                    break;
                case HashAlgorithmType.MD5:
                default:
                    hashSizeInBits = 128;
                    break;
            }

            var hashSizeInBytes = hashSizeInBits / 8;
            if (hashWithSaltBytes.Length < hashSizeInBytes)
                return string.Empty;

            var saltBytes = new byte[hashWithSaltBytes.Length -
                                     hashSizeInBytes];

            for (var i = 0; i < saltBytes.Length; i++)
                saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

            return Hash(plainText, hashAlgorithm, saltBytes);
        }

        public static string Encrypt(this string stringToEncrypt, string key, bool isStrict = true)
        {
            if ((stringToEncrypt == null || stringToEncrypt.Equals(string.Empty)) && !isStrict) return string.Empty;
            if (stringToEncrypt == null || stringToEncrypt.Equals(string.Empty))
                throw new ArgumentNullException(nameof(stringToEncrypt), @"String to encrypt cannot be null or empty");
            if (key == null)
                throw new ArgumentNullException(nameof(key), @"Key cannot be null");

            var toEncryptArray = Encoding.UTF8.GetBytes(stringToEncrypt);

            var hashed5 = new MD5CryptoServiceProvider();
            var keyArray = hashed5.ComputeHash(Encoding.UTF8.GetBytes(key));
            hashed5.Clear();

            var teds = new TripleDESCryptoServiceProvider
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            var cTransform = teds.CreateEncryptor();
            var resultArray =
                cTransform.TransformFinalBlock(toEncryptArray, 0,
                    toEncryptArray.Length);
            teds.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(this string stringToDecrypt, string key, bool isStrict = true)
        {
            if ((stringToDecrypt == null || stringToDecrypt.Equals(string.Empty)) && !isStrict) return string.Empty;
            if (stringToDecrypt == null || stringToDecrypt.Equals(string.Empty))
                throw new ArgumentNullException(nameof(stringToDecrypt), @"String to decrypt cannot be null or empty");
            if (key == null)
                throw new ArgumentNullException(nameof(key), @"Key cannot be null");

            var toEncryptArray = Convert.FromBase64String(stringToDecrypt);

            var hashed5 = new MD5CryptoServiceProvider();
            var keyArray = hashed5.ComputeHash(Encoding.UTF8.GetBytes(key));
            hashed5.Clear();

            var teds = new TripleDESCryptoServiceProvider
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            var cTransform = teds.CreateDecryptor();
            var resultArray = cTransform.TransformFinalBlock(
                toEncryptArray, 0, toEncryptArray.Length);
            teds.Clear();
            return Encoding.UTF8.GetString(resultArray);
        }

        public static string Encrypt<THashAlgorithm, TSymmetricAlgorithm>(
            this string value, string key,
            CipherMode cipherMode = CipherMode.ECB,
            PaddingMode paddingMode = PaddingMode.PKCS7,
            Encoding encoding = null)
            where THashAlgorithm : HashAlgorithm, new()
            where TSymmetricAlgorithm : SymmetricAlgorithm, new()
        {
            if (key.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(key), "Key cannot be null");

            if (value.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(value), "String to encrypt cannot be null");

            encoding = encoding ?? Encoding.UTF8;

            var keyHash = encoding.GetBytes(key);
            var valHash = Convert.FromBase64String(value);

            var hashAlgo = new THashAlgorithm();
            var symmAlgo = new TSymmetricAlgorithm
            {
                Key = hashAlgo.ComputeHash(keyHash),
                Mode = cipherMode,
                Padding = paddingMode
            };
            hashAlgo.Clear();

            var encryptor = symmAlgo.CreateEncryptor();
            var encryptedBytes = encryptor.TransformFinalBlock(valHash, 0, valHash.Length);
            symmAlgo.Clear();

            return Convert.ToBase64String(encryptedBytes, 0, encryptedBytes.Length);
        }

        public static string Decrypt<THashAlgorithm, TSymmetricAlgorithm>(
            this string value, string key,
            CipherMode cipherMode = CipherMode.ECB,
            PaddingMode paddingMode = PaddingMode.PKCS7,
            Encoding encoding = null)
            where THashAlgorithm : HashAlgorithm, new()
            where TSymmetricAlgorithm : SymmetricAlgorithm, new()
        {
            if (key.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(key), "Key cannot be null");

            if (value.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(value), "String to encrypt cannot be null");

            encoding = encoding ?? Encoding.UTF8;

            var keyHash = encoding.GetBytes(key);
            var valHash = Convert.FromBase64String(value);

            var hashAlgo = new THashAlgorithm();
            var symmAlgo = new TSymmetricAlgorithm
            {
                Key = hashAlgo.ComputeHash(keyHash),
                Mode = cipherMode,
                Padding = paddingMode
            };
            hashAlgo.Clear();

            var decryptor = symmAlgo.CreateDecryptor();
            var decryptedBytes = decryptor.TransformFinalBlock(valHash, 0, valHash.Length);
            symmAlgo.Clear();

            return encoding.GetString(decryptedBytes);
        }
    }
}